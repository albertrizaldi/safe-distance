\section{Designing Sound Checkers for the Safe Distance Rule}
\label{sec:checker}

We use the analyses from Sec.~\ref{sec:case_analysis} to guide the design of
sound and complete abstract checkers in Sec.~\ref{subsec:abstract}; these
checkers are defined in terms of real numbers and other non-executable
constructs. We then show how to turn them into executable checkers by using
exact rational arithmetic, symbolic decision procedures, or interval
arithmetic in Sec.~\ref{subsec:exec_chk}.

\subsection{Abstract Checkers}
\label{subsec:abstract}

%We can perform two kinds of case distinctions to deal with the hybrid nature of
%the problem: the two different case distinctions from
%Sec.~\ref{sec:case_analysis} according to stopping position or collision time
%lead to different formulations of the checker: one prescriptively $\Ichecker_p$
%(including a quantifier over time), the other descriptively (without the
%quantifier).

%We have performed two case distinctions in Sec.~\ref{sec:case_analysis}: based on
%stopping times and stopping distances. A descriptive checker is derived from 
%the former and a prescriptive checker is derived from the latter.

We design two checkers here: a descriptive and a prescriptive version. 
Both checkers are derived from the case distinction in Sec.~\ref{sec:case_analysis1} and
Sec.~\ref{sec:analysis}, respectively.

\subsubsection{Descriptive Checker}
%By summarising the case analysis on which of the cars
%has stopped already (Section~\ref{sec:case_analysis1}), the problem of
%determining whether there is a collision can be reduced to determining whether
%the following polynomials have roots in a given interval, where $\Ihasroot$
%is defined as $f(t)~\Ihasroot~T \longleftrightarrow \exists t \in T.~f(t) = 0$. 
%
From the case distinction based on stopping times in Sec.~\ref{sec:case_analysis1}, we
conclude that the problem of detecting collision is reduced into the
problem of finding solutions for each entry in Tab.~\ref{tab:case_analysis1} in the
corresponding time interval. This is formalised with the predicate $\Ihasroot$, 
defined as
$f(t)\ \Ihasroot\ T \ \longleftrightarrow \ \exists t \in T. \ f(t) = 0$.
A checker based on this approach can then be defined as follows.
\[
\Ichecker_{\text{d}}  :=  \neg\left(
  \begin{aligned}
    p_\text{e}(t) - p_\text{o}(t)                       &~\Ihasroot~[0; \min \, \{t_{\text{stop,e}}, t_{\text{stop,o}}\}] &~\lor \\ 
    p_{\text{e}}(t_{\text{stop,e}}) - p_\text{o}(t)     &~\Ihasroot~[t_{\text{stop,e}}; t_{\text{stop,o}}]                &~\lor \\
    p_{\text{o}}(t_{\text{stop,o}}) - p_\text{e}(t)     &~\Ihasroot~[t_{\text{stop,o}}; t_{\text{stop,e}}]                &~\lor \\
    p_{\text{e}}(t_{\text{stop,e}}) - p_{\text{o}}(t_{\text{stop,o}}) &~\Ihasroot~[\max \, \{t_{\text{stop,e}}, t_{\text{stop,e}}\}; \infty)
 \end{aligned}
\right)
\]
%
The following theorem ensures that the checker is both sound and complete. 
It follows immediately from the definitions of braking movement $p$, 
stopping time $t_{\text{stop}}$, predicate $\Ihasroot$, and predicate 
$\Icollision$:
\begin{theorem}[Correctness of abstract descriptive checker]
\label{thm:correct-first-checker}
\[\Ichecker_{\text{\rm d}} \ \Longleftrightarrow \ \neg \Icollision \, [0; \infty)\]
\end{theorem}

\subsubsection{Prescriptive Checker}
%The analysis according to stopping location (Section~\ref{sec:analysis}), in
%particular the different conditions in
%Theorems~\ref{thm:cond_1}, \ref{thm:cond_2}, and \ref{thm:cond_3}
%yield a descriptive characterization of safe distance:
%\[ \Ichecker_d :=  cond_1 \land cond_2 \land cond_3 \]
%which guarantees collision freedom. The second conjunct is the negation of the
%condition in Theorem~\ref{thm:cond_2}, and \ref{thm:cond_3}
From the case distinction based on stopping distances in Sec.~\ref{sec:analysis}, we
have defined three expressions of safe distances. Each expression has associated
preconditions for which the expression is valid. We can design the prescriptive
checker from these expressions as follows:
\[ 
\begin{aligned} 
\Ichecker_{\text{p}} \ := \ & \Klet  \ \identifier{dist} = s_{0,\text{o}} - s_{0,\text{e}}\ \Kin\\
                   & \Kif \ \underline{\identifier{dist} \, > \,
\identifier{safe-distance}}_1 \ \Kthen \ \identifier{True} \\ 
                   & \Kelse \, \Kif \ a_{\text{0}} > a_{\text{e}} \land
v_{\text{o}} < v_{\text{e}} \land t_{\text{stop,e}} < t_{\text{stop,o}}\ \Kthen
\ \underline{\identifier{dist} \, > \, \identifier{safe-distance}}_3 \\ 
                   & \Kelse \ \underline{\identifier{dist} \, > \,
\identifier{safe-distance}}_2
\end{aligned}
\]

%The preconditions being a complete case distinction, the following correctness
%theorem of the descriptive checker follows immediately:
\noindent The following theorem states that the prescriptive checker is also sound and
complete. 
\begin{theorem}[Correctness of abstract prescriptive checker]
\label{thm:correct-second-checker}
\[\Ichecker_{\text{\rm p}} \ \Longleftrightarrow \ \neg \Icollision\, [0; \infty)\]
\end{theorem}
\begin{proof}
The soundness follows from the Theorem~\ref{thm:cond_1}, \ref{thm:cond_2}, and
\ref{thm:cond_3} in Sec.~\ref{sec:case_analysis} while the completeness comes from the 
fact that case~\circled{1}, \circled{2}, and \circled{3} cover all possible cases. \qed
\end{proof}

\subsection{Executable Checkers}
\label{subsec:exec_chk}

A fragment of HOL can be seen as a functional programming language. When we talk
about \emph{executable} specifications, we talk about specifications within that
fragment. In principle, such specifications could be evaluated inside
Isabelle's kernel. For a more efficient evaluation, Isabelle/HOL comes with a
code generator~\cite{Haftmann2010}, which translates executable specifications
to code for (functional) programming languages like SML, OCaml, Scala, or
Haskell. We will generated the code for SML to evaluate the US-101 Highway data set
in Sec.~\ref{sec:experiment}.

The aforementioned checkers $\Ichecker_{\text{d}}$ and $\Ichecker_{\text{p}}$
are formally proved correct, but are not executable, because they involve e.g.,
real numbers or quantifiers over real numbers (via $\Ihasroot$). We therefore
refine them towards executable formulations. To this end, Isabelle provides a
variety of techniques, and we explore the use of the following:
\begin{enumerate}
\item \emph{Exact arithmetic on rational numbers.}\\
Exact arithmetic on rational numbers can be directly used for 
$\Ichecker_{\text{p}}$ if all parameters are rational numbers. 
It requires, however, the manual work of formalising the analysis presented in
Section~\ref{sec:analysis}.
\item \emph{Decision procedure for finding roots of univariate polynomials.}\\ 
By contrast, using a decision procedure based on Sturm sequences for $\Ihasroot$
in $\Ichecker_{\text{d}}$ requires almost no manual reasoning. However, it has
to be used as a black-box method and might not be easy to extend, if it is
required.  
\item \emph{Interval arithmetic.}\\
With interval arithmetic, one can include uncertainties into
parameters of the model and could even %attack 
address non-polynomial problems. Numerical uncertainties can, however, 
%lead to
cause the checkers to be incomplete.
\end{enumerate}

\paragraph{Exact rational arithmetic.}
All the operations occurring in $\Ichecker_{\text{p}}$ could be executed on
rational numbers. Under the assumption that all the parameters are rational
numbers, $\Ichecker_{\text{p}}$ can be executed using the standard approach of
\emph{data-refinement}~\cite{Haftmann2013} for real numbers in Isabelle/HOL.
That is, the code generator is instructed to represent real numbers as a data
type with a constructor $\IRatreal: \Rat \to \Real$. Then, operations on the
rational subset of the real numbers are defined by pattern matching on the
constructor, and performing the corresponding operation on rational numbers.
For example, addition $+_{\Real}$ on $\IRatreal$-constructed real numbers can be
implemented with addition $+_{\Rat}$ on rational numbers: $\IRatreal(p)
+_{\Real} \IRatreal(q) = \IRatreal(p +_{\Rat} q)$. Therefore, as long as the
input is given as rational numbers, code generation for $\Ichecker_{\text{p}}$
works without further manual setup. Correctness follows from
Theorem~\ref{thm:correct-second-checker}.

\paragraph{Sturm sequences.}
A different approach can be followed by looking at the prescriptive formulation
$\Ichecker_{\text{d}}$. To evaluate $\Ihasroot$, we can resort to a decision procedure
based on \emph{Sturm sequences} which been formalised in
Isabelle~\cite{Eberl2015}. The interface to this decision procedure is an
executable function $\Icountroots(p, I)$, which returns the number of roots of a
given univariate polynomial $p$ in a given interval $I$. It satisfies the
proposition $p~\Ihasroot~T \longleftarrow (\Icountroots(p,I) > 0)$ and can
therefore be used as an executable specification for the occurrences of
$\Ihasroot$ in $\Ichecker_{\text{d}}$.  Correctness follows from
Theorem~\ref{thm:correct-first-checker}.


\paragraph{Interval arithmetic.}
\label{subsec:concrete}
The previous two approaches both assume that the parameters are given as exact
rational numbers. One could argue that this is an unrealistic assumption,
because real-world data cannot be measured exactly. For this checker, we
therefore allow intervals of parameters.
Isabelle's \emph{approximation}~\cite{hoelzl09realineequalities} method allows us
to interpret $\Ichecker_{\text{p}}$ (a formula with inequalities over real
numbers) as an expression in interval arithmetic. The resulting checker
$\Ichecker_{\text{i}}$ takes a Cartesian product of intervals as enclosure for
the parameters as input.

\begin{theorem}[Correctness of Checker]\label{thm:correct-third-checker} \\ If $(s_\text{\rm e}, v_\text{\rm e}, a_\text{\rm e}, s_\text{\rm o}, v_\text{\rm o}, a_\text{\rm o})\in
S_\text{\rm e} \times V_\text{\rm e} \times  A_\text{\rm e} \times S_\text{\rm o} \times V_\text{\rm o} \times A_\text{\rm o}$, then
\[
\Ichecker_{\text{\rm i}} \, (S_\text{\rm e}, V_\text{\rm e}, A_\text{\rm e}, S_\text{\rm o}, V_\text{\rm o}, A_\text{\rm o})  \ \implies \ \neg \Icollision [0;\infty)
\]
\end{theorem}
\begin{proof}
The theorem follows directly from the correctness of \emph{approximation}. \qed
\end{proof}
Note that we lose completeness in this approach; the checker could fail to prove
collision-freedom because of imprecision in the approximate calculations. Such
imprecision occurs because of, e.g., finite precision calculations or case
distinctions that cannot be resolved. It might be that in a case distinction $a
< b \lor a \ge b$, none of the two disjuncts can be proved (consider e.g.\ $a
\in [0; 1], b \in [0; 1]$) with just interval arithmetic. Tracking dependencies
between input variables or interval constraint propagation approaches could
alleviate this problem.
