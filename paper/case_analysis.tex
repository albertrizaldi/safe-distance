\section{Logical Analysis of the Safe Distance Problem}
\label{sec:case_analysis}
This section analyses the safe distance problem by performing two case distinctions
based on stopping times and stopping distances. 
%This analysis
%allow us to obtain a prescriptive definition of safe distance and to design
%checkers later in Sec.~\ref{sec:checker}.
The first case distinction (Sec.~\ref{sec:case_analysis1}) is more suitable for
\emph{checking} whether there will be a collision or not. The second case
distinction (Sec.~\ref{sec:analysis}) meanwhile is about eliminating the
existential quantifier in Eq.~(\ref{eq:collision_def}) and rearranging the
resulting formula such that one can obtain lower bounds for the initial
distance $s_{0,\text{o}} - s_{0,\text{e}}$ that is still safe.  In principle,
after resolving the discrete jumps, this quantifier elimination for real
arithmetic could be achieved by automatic procedures as implemented in modern
computer algebra systems (CASs).  There is even a proof-producing procedure
implemented in the HOL-Light theorem prover~\cite{McLaughlin2005}.  However,
our seven-variable formula appears to be too complex for HOL-Light's quantifier
elimination procedure.  Therefore, manually finding this lower bound with an
interactive theorem prover is necessary.
%---even if we decided to use HOL-Light.
This makes our results more robust against changes in the formalisation and
more readable compared to those from CASs'.

\subsection{Case Distinction Based on Stopping Times}
\label{sec:case_analysis1}
To check for collisions, we need to find the solution of Eq.~\eqref{eq:collision_def}.
However, finding the solution is problematic due to the occurrences of the
\textsf{if}-construct in the definition of the overall movement
in Eq.~\eqref{eq:overall_movement}. Therefore, we perform case distinction based on two
stopping times conditions: $0 \leq t \leq t_{\text{stop}}$ and
$t_{\text{stop}} < t$ for both vehicles. This produces four cases in total
(see Tab.~\ref{tab:case_analysis1}). Each case is the equation where
$s_{\text{e}}(t) = s_{\text{o}}(t)$ with functions $s_{\text{e}}(t)$ and $s_{\text{o}}(t)$ 
are substituted as in Eq.~\eqref{eq:overall_movement}, depending on which 
stopping time condition holds.  Since each case is a pure quadratic equation,
we can use a decision procedure for finding roots of univariate polynomials for
each case.  A checker based on such a decision procedure is described in
Sec.~\ref{subsec:abstract}.

\begin{table}[tb]
\centering
\caption{Four cases of stopping times and the corresponding equations of
$s_{\text{e}}(t) = s_{\text{o}}(t)$.} \label{tab:case_analysis1}
\ra{1.3}
\begin{tabular}{@{}ccp{0.3\textwidth}p{0.3\textwidth}@{}}\toprule
                                  &\phantom{abcd} &   $0 \leq t \leq t_{\text{stop,e}}$      & $t_{\text{stop,e}} < t$ \\
\midrule
$0 \leq t \leq t_{\text{stop,o}}$ &&   \textsuperscript{\circled{a}}$p_{\text{e}}(t) = p_{\text{o}}(t)$                 & \textsuperscript{\circled{b}}$p_{\text{e}}(t_{\text{stop,e}}) = p_{\text{o}}(t)$\\
$t_{\text{stop,o}} < t$           &&   \textsuperscript{\circled{c}}$p_{\text{e}}(t) = p_{\text{o}}(t_{\text{stop,o}})$ & \textsuperscript{\circled{d}}$p_{\text{e}}(t_{\text{stop,e}}) = p_{\text{o}}(t_{\text{stop,o}})$\\
\bottomrule
\end{tabular}
\end{table}

\newcommand{\Iprecond}{\identifier{precondition}}
\subsection{Case Distinction Based on Stopping Distances}
\label{sec:analysis}
\begin{figure}[tbp]
\centering
\input{./tikzfigure/case_analysis.tex}
\caption{Three cases obtained from case distinction based on stopping distances.}
\label{fig:case_analysis2}
\end{figure}

Figure~\ref{fig:case_analysis2} illustrates the case distinction
based on stopping distances. It plots an example of overall movement for the
other vehicle $s_{\text{o}}(t)$ and divides this movement into three regions (cases)
where a stopping distance of the ego vehicle
$s_{\text{stop,e}} \ := \ s_{\text{e}}(t_{\text{stop,e}})$
could be located:
\begin{itemize}
\item [\circled{1}.] $s_{\text{stop,e}}  < s_{0,\text{o}}$;
\item [\circled{2}.] $s_{\text{stop,o}} \leq s_{\text{stop,e}}$.
\item [\circled{3}.] $s_{0,\text{o}} \leq s_{\text{stop,e}} < s_{\text{stop,o}}$.
\end{itemize}
These stopping distances can be obtained  by substituting $t_{\text{stop}}$ in
Eq.~\eqref{eq:tstop} to $s$ in Eq.~\eqref{eq:overall_movement} for the ego and
the other vehicle as follows: 
\begin{equation}\label{eq:sstop}
    s_{\text{stop,e}} \ = \ s_{0,\text{e}} - \frac{v_{\text{e}}^2}{2 \cdot a_{\text{e}}}
    \quad \text{and} \quad
    s_{\text{stop,o}} \ = \ s_{0,\text{o}} - \frac{v_{\text{o}}^2}{2 \cdot
a_{\text{o}}} \enspace .
\end{equation}
%Thus, 
For any case in which collision freedom can be deduced, we rearrange
the terms and the deduction into the following pattern:
\begin{multline}\label{eq:pattern}
s_{\text{0,o}} - s_{\text{0,e}} \, > \, \Isafedistance\,(a_{\text{e}}, v_{\text{e}}, a_{\text{o}}, v_{\text{o}}) \ \Longrightarrow
\ \Iprecond \, (a_{\text{e}}, v_{\text{e}}, a_{\text{o}}, v_{\text{o}}) \ \Longrightarrow \\ \neg \, \Icollision \, [0;\infty) 
\enspace .
\end{multline}
This pattern has the interpretation that if the initial distance $s_{0,\text{o}}
- s_{0,\text{e}}$ is bigger than the expression $\Isafedistance\,(a_{\text{e}}, v_{\text{e}}, a_{\text{o}},
  v_{\text{o}})$ and the $\Iprecond\,(a_{\text{e}}, v_{\text{e}}, a_{\text{o}},
  v_{\text{o}})$ holds, too, then we can guarantee that there
will be no collision. We claim that the expression
$\Isafedistance \, (a_{\text{e}}, v_{\text{e}},
a_{\text{o}}, v_{\text{o}})$ defines the notion of safe distance \emph{prescriptively};
one can easily check whether a collision exists by comparing the initial
distance with this expression.

In the rest of this section, we prove three theorems---one for each of these
three cases---which determine whether there is a
collision or not. As an overview, collision freedom can be deduced in case
\circled{1} while collision can be deduced in case \circled{2}. In
case~\circled{3}, collision depends on further conditions than just the premise
$s_{0,\text{o}} \leq s_{\text{stop,e}} < s_{\text{stop,o}}$.
%neither collision nor collision freedom can be deduced from
%the premise $s_{0,\text{o}} \leq s_{\text{stop,e}} < s_{\text{stop,o}}$ only.
%We therefore provide additional sufficient and necessary conditions from which
%we can deduce whether a collision occurs in case \circled{3}. 

\paragraph{Background formalisation.} 
\newcommand{\tstop}{t_{\text{stop}}}

Consider the quadratic equations of the form $p(x) := ax^2 + bx + c$
with the discriminant $D := b^2 - 4ac$.  
The analysis of the movement can be carried out with the following well-known 
mathematical facts about quadratic forms. 
\begin{itemize}
\item [--] Solution of quadratic equation:
\begin{equation}\label{eqn:solution_defs} D \ge 0 \ \implies \ x_{1,2} \, := \, \frac{-b \pm \sqrt{D}}{2a} \end{equation}
\begin{equation}\label{eqn:root_iff_roots} a \ne 0 \ \implies \ p(x) = 0 \ \iff \ \left(D \ge 0 \, \land \, \left(x = x_1 \, \lor \, x = x_2\right)\right) \end{equation}
\item [--] Condition for convexity:
\begin{equation} \label{eqn:convex_p}x < y < z \ \ \land \ \ p(x) > p(y) \le p(z) \ \ \implies \ \ a > 0 \end{equation}
\item [--] Monotonicity:
\begin{equation}\label{eq:mono_s}
(t \le u \implies s(t) \le s(u)) \land
  (t < u \land u \le \tstop \implies s(t) < s(u))\end{equation}
\item [--] Maximum at the stopping time:
  \begin{equation}\label{eqn:max_stop}
  p(t) \le p(\tstop) \quad \land \quad s(t) \le s(\tstop)
  \end{equation}
\end{itemize}
Because these are all basic, well-known facts, one can expect that the overhead
of using a theorem prover be kept within limits. Indeed, %We confirm this expectation:
all of the facts in Eq.~\eqref{eqn:solution_defs} to~\eqref{eqn:max_stop} can
be proved automatically with one of Isabelle's automatic provers: the
sum-of-squares methods (ported from Harrison~\cite{HarrisonSOS}) or rewriting of
arithmetic expressions combined with classical reasoning.
%\todo{Check if that is really the case!}
%\todos{all of these equations into a separate figure or sth like that?}

\subsubsection{Theorems}
We start with the first theorem for case \circled{1} which states that this case
implies collision freedom. Intuitively speaking, there will be no collision in
this case because the ego vehicle is located so far that it stops before the
initial position of the other vehicle.
\begin{theorem}[Obvious collision freedom in case \circled{1}]
\label{thm:cond_1}
%If the stopping position of the ego
%vehicle is less than the initial position of the other vehicle, then there will
%be no collision. 
\begin{equation}
 s_{\text{\rm stop,e}} < s_{0, \text{\rm o}} \implies \neg \, \Icollision \,
[0;\infty)
\end{equation}
\end{theorem}
\begin{proof}
This is true because $s_{\text{e}}(t) < s_{\text{o}}(t)$ holds for
every time $t \ge 0$: $s_{\text{e}}(t) \le s_{\text{e}}(t_{\text{stop,e}}) =
s_{\text{stop,e}} < s_{0,\text{o}} = s_{\text{o}}(0)\le s_{\text{o}}(t)$ due
to transitivity, the assumption, and monotonicity of~$s$
in Eq.~\eqref{eq:mono_s}, and the maximum at the stopping time
in Eq.~\eqref{eqn:max_stop}. \qed
\end{proof}

\noindent Since this case implies absence of collision, we can unfold the definition 
of $s_{\text{stop,e}}$ in Eq.~\eqref{eq:sstop} and rearrange Theorem~\ref{thm:cond_1}
according to the pattern in Eq.~(\ref{eq:pattern}) into the following safe
distance expression:
%\footnote{Note that there is no expression for the $\Iprecond$. This means that a distance larger than $\Isafedistance_1$ always ensures
%a collision freedom.}. 
\begin{equation}\label{eq:safedistance1}
\Isafedistance_1 := - \frac{v^2_{\text{e}}}{2 \cdot a_{\text{e}}}
\end{equation}

For case \circled{2}, we first give the following lemma which provides a
sufficient condition for a collision in a bounded interval.  
It follows directly from the continuity of $s$ and an application of the
intermediate value theorem for $s_{\text{o}} -
s_{\text{e}}$ between $0$ and $t$.
\begin{lemma}[Upper bounds on collision time]
\label{thm:guaranteed_collision}
%For every time $t \, :: \, \Real$ where $s_{\text{\rm e}}(t) 
%\geq s_{\text{\rm o}}(t)$, there is a collision in the interval $[0;t]$.
\begin{equation*}
    (s_{\text{\rm e}}(t) \ge s_{\text{\rm o}}(t) \, \implies \, \Icollision \,
    [0;t])
    \ \land \
    (s_{\text{\rm e}}(t) > s_{\text{\rm o}}(t) \, \implies \, \Icollision \,
    [0;t))
\end{equation*}
\end{lemma}
\noindent Then, the following theorem states that case \circled{2} necessarily
implies a collision.  
\begin{theorem}[Obvious collision in case \circled{2}]
\label{thm:cond_2}
\[ s_{\text{\rm stop,e}} \ge
   s_{\text{\rm stop,o}} \implies \Icollision \, [0;\infty)\]
\end{theorem}
\begin{proof}
Since by definition $s_{\text{\rm stop,e}} = s_{\text{e}}(t_{\text{stop,e}})$
and $s_{\text{\rm stop,o}} = s_{\text{o}}(t_{\text{stop,o}})$, setting
$t := \max \, \{t_{\text{stop,e}}, t_{\text{stop,o}}\}$ 
in Lemma~\ref{thm:guaranteed_collision} above proves that this case implies 
a collision. \qed
% + Since $s_{\text{\rm stop,e}}$ and and $s_{\text{\rm stop,o}}$ are both attained
% + at $\max \, (t_{\text{stop, e}}, t_{\text{stop, o}})$, we can apply
% + Lemma~\ref{thm:guaranteed_collision}. \qed
\end{proof}
Since case \circled{2} implies collision, no safe distance
expression is produced from the logical analysis of case \circled{2}.

We now consider case \circled{3}, where the ego vehicle stops behind the
other vehicle. There can still be a collision, i.e. the movement of the ego
vehicle can intersect the movement of the other vehicle and still 
stop behind the other vehicle (see Fig.~\ref{fig:case_analysis2}).
The following lemma states that  a collision (if any) in case \circled{3} must
occur while \emph{both} cars are still moving. This lemma therefore allows us to 
reduce the reasoning to the continuous part $p$ of the movement $s$.
\begin{lemma}[Collision within stopping times in case \circled{3}]
\label{lemma:collision_within_eq} 
\begin{multline}\label{eq:collision_within_eq}
s_{0,\text{\rm o}} \leq s_{\text{\rm stop,e}} < s_{\text{\rm stop,o}} \
\Longrightarrow \\
\Icollision \, [0;\infty) \ \Longleftrightarrow \
\Icollision \ 
\left(0; \min \, \{t_{\text{\rm stop,e}}, \; t_{\text{\rm stop,o}}\}\right) 
\end{multline}
\end{lemma}
\begin{proof} 
The ``$\Longleftarrow$''-part is obvious and we only prove the ``$\Longrightarrow$''-part.  
If a collision happens at time $t$ while one of the vehicles has already stopped,
then it must be the ego vehicle which has stopped ($t_{\text{\rm stop,e}} < t$). Also,
we have $s_{\text{e}}(t_{\text{\rm stop,e}}) > s_{\text{o}}(t_{\text{\rm
stop,e}})$ 
%because we are in case \circled{3}.  
because according to Eq.~(\ref{eq:mono_s}), $s_{\text{o}}$ is strictly increasing in $[t_{\text{stop,e}}; t]$ (see Fig.~\ref{fig:case_analysis2}).
Then, Lemma~\ref{thm:guaranteed_collision} yields a suitable witness
for an earlier collision $t' < t_{\text{stop,e}}$. The whole proof takes just about 80 lines in the
formalisation. \qed
\end{proof}

\noindent Then, the following theorem characterises the conditions for ensuring
a collision in case~\circled{3}.
\begin{theorem}[Conditional collision in case \circled{3}]\label{thm:cond_3}
%When the conditions $s_{0,\text{\rm o}} \leq  s_{\text{\rm stop,e}}$ and
%$s_{\text{\rm stop,e}} \leq s_{\text{\rm stop,o}}$ are satisfied, 
%it is guaranteed that $\Icollision \, [0;\infty)$ holds if and only if the
%following condition
%is satisfied.
\begin{multline}\label{eq:conjuncts}
s_{0,\text{\rm o}} \leq s_{\text{\rm stop,e}} < s_{\text{\rm stop,o}} \
\Longrightarrow \ \Icollision \, [0;\infty) \ \Longleftrightarrow \\
     a_{\text{\rm o}} > a_{\text{\rm e}}  \ \wedge \ 
      v_{\text{\rm o}} < v_{\text{\rm e}} \ \wedge \ 
      s_{0, \text{\rm o}} - s_{0, \text{\rm e}} \leq 
      \frac{(v_{\text{\rm o}} - v_{\text{\rm e}})^2}{2 \cdot (a_{\text{\rm o}} - a_{\text{\rm e}})}
      \ \wedge \
      t_{\text{\rm stop,e}} < t_{\text{\rm stop,o}} 
\end{multline}
\end{theorem}
\begin{proof}
\emph{(``$\Longleftarrow$''.)} Case~\circled{3} and
Eqs.~\eqref{eqn:solution_defs}, \eqref{eqn:root_iff_roots}, and
\eqref{eq:conjuncts} yield a root of $p_{\text{o}} - p_{\text{e}}$, which is contained in the
interval $(0 ; \min\{t_{\text{\rm stop,e}},t_{\text{\rm stop,o}}\})$. The root is
therefore also a root of $s_{\text{o}} - s_{\text{e}}$ and therefore witnesses $\Icollision \,
[0;\infty)$. \\

\noindent \emph{Only if (``$\Longrightarrow$'')-part of the conclusion.}
From $\Icollision \, [0;\infty)$, we
obtain a root $t$ with $s_{\text{o}}(t) - s_{\text{e}}(t) = 0$.
Then, Lemma~\ref{lemma:collision_within_eq} allows us to deduce
$p_{\text{o}}(t) - p_{\text{e}}(t) = 0$ and condition~\eqref{eqn:convex_p} for
convexity (for $p_{\text{o}} - p_{\text{e}}$  at times
$0 < t < \min\{t_{\text{\rm stop,e}},t_{\text{\rm stop,o}}\}$) yields $a_{\text{o}} > a_{\text{e}}$.
This gives, together with the fact that the discriminant of $p_{\text{o}} -
p_{\text{e}}$ is nonnegative according to Eq.~\eqref{eqn:root_iff_roots},
the remaining conjuncts of Eq.~\eqref{eq:conjuncts} after some arithmetic
manipulations and reasoning. The whole proof takes about 130 lines in the
formalisation. \qed
\end{proof}
\noindent In order to unify this theorem with the pattern in~\eqref{eq:pattern}, 
we negate the logical equivalence in~\eqref{eq:conjuncts} and rearrange the
theorem as follows.
\begin{multline}\label{eq:case_3_no_coll}
s_{0,\text{o}} - s_{0,\text{e}} > 
\frac{v^2_{\text{o}}}{2 \cdot a_{\text{o}}} - \frac{v^2_{\text{e}}}{2 \cdot
a_{\text{e}}} \implies \
s_{0,\text{o}} - s_{0,\text{e}} > 
\frac{(v_{\text{o}} - v_{\text{e}})^2}{2 \cdot (a_{\text{o}} - a_{\text{e}})}
\implies \
s_{0,\text{o}} \leq s_{\text{stop,e}} \ \implies \\ 
      \left(a_{\text{\rm o}} > a_{\text{\rm e}}  \ \wedge \ 
      v_{\text{\rm o}} < v_{\text{\rm e}} \ \wedge \ 
      t_{\text{\rm stop,e}} < t_{\text{\rm stop,o}}\right)
\ \Longrightarrow \
\neg \, \Icollision \, [0;\infty)
\end{multline}
This reformulation fits the pattern in~\eqref{eq:pattern} and now we have
two possible safe distance expressions and one for precondition:
\begin{multline}\label{eq:safedistance23}
\Isafedistance_2 \, := \, 
\frac{v^2_{\text{o}}}{2 \cdot a_{\text{o}}} - \frac{v^2_{\text{e}}}{2 \cdot a_{\text{e}}}
\qquad  
\identifier{safe-distance}_3 \, := \,
\frac{(v_{\text{o}} - v_{\text{e}})^2}{2 \cdot (a_{\text{o}} - a_{\text{e}})}  \enspace,\\
\identifier{precondition} \ := \ 
s_{0,\text{o}} \leq s_{\text{stop,e}} \ \land \ 
      \left(a_{\text{\rm o}} > a_{\text{\rm e}}  \ \wedge \ 
      v_{\text{\rm o}} < v_{\text{\rm e}} \ \wedge \ 
      t_{\text{\rm stop,e}} < t_{\text{\rm stop,o}}\right)
\end{multline}
To choose between these two expressions, we use the following lemma 
which determines their relative position.
\begin{lemma}[Relative position of safe distance expressions]
\[
a_{\text{\rm o}} > a_{\text{\rm e}} \ \Longrightarrow \
    \identifier{safe-distance}_2 \, \leq \, \identifier{safe-distance}_3
\]
\end{lemma}
\begin{proof}
We prove this lemma by multiplying both sides with the multiplier
$2\cdot(a_{\text{0}} - a_{\text{e}})$ which is positive. 
Then, we reason backwards by performing arithmetical reasoning which eventually
leads to $0 \leq (t_{\text{stop,e}} - t_{\text{stop,o}})^2$ which is always true. \qed 
\end{proof}
With this lemma, we choose $\Isafedistance_3$ when the $\Iprecond$ holds. Otherwise,
it must be the case that $\neg \left(a_{\text{\rm o}} > a_{\text{\rm e}}  \
\wedge \ v_{\text{\rm o}} < v_{\text{\rm e}} \ \wedge \ t_{\text{\rm stop,e}} <
t_{\text{\rm stop,o}}\right)$---since we assume case~\circled{3}.  Then,
Theorem~\ref{thm:cond_3} ensures that $\Isafedistance_2$ is indeed a prescriptive
definition of safe distance. 

\paragraph{Overall definition.}
To sum up our logical analysis, $\Isafedistance_1$ always holds as 
a prescriptive definition of the safe distance. Expression $\Isafedistance_3$ 
holds when it is case~\circled{3} and $\Iprecond$ holds, while  
$\Isafedistance_2$ is valid when it is still case~\circled{3} but $\Iprecond$
does not hold. 

