\section{Formalising Safe Distance from the Vienna Convention}
\label{sec:model}

\begin{figure}[tbp]
\input{./tikzfigure/scenario.tex}
\caption{Scenario for safe distance problem.}
\label{fig:scenario}
\end{figure}

Figure~\ref{fig:scenario} illustrates the scenario for the safe distance problem
as defined in the Vienna Convention on Road Traffic. The scenario consists of two
vehicles: the \emph{ego} vehicle and the closest vehicle in front of it---which we
term \emph{other} vehicle\footnote{NGSIM has identified the other vehicle for
each ego vehicle in the US-101 Highway data set.}.
%The rest of vehicles located
%behind the ego vehicle, in front of the other vehicle, and in other lanes are
%ignored.
This scenario  is uniquely characterised by six constants:
$s_{0,\text{e}}, v_{\text{e}}, a_{\text{e}} \, \in \, \Real$
from the ego vehicle and $s_{0,\text{o}}, v_{\text{o}}, a_{\text{o}} \, \in \,
\Real$ from the other vehicle. Constants $s_{0}, v, a$ denote the initial
position, initial speed, and maximum deceleration value, respectively, of a
vehicle.  Note that $s_{0,\text{e}}$ denotes the \emph{frontmost} position of
the ego vehicle, while $s_{0,\text{o}}$ denotes the \emph{rearmost} position of
the other vehicle.  Additionally, we also make the following assumptions:
\begin{assumption} \label{assm:1}
The values of $v_{\text{\rm e}}$ and $v_{\text{\rm o}}$ are non-negative:
$0 \leq v_{\text{\rm e}} \ \land \ 0 \leq v_{\text{\rm o}}$.
\end{assumption}
\begin{assumption} \label{assm:2}
The values of $a_{\text{\rm e}}$ and $a_{\text{\rm o}}$ are negative:
$a_{\text{\rm e}} < 0 \ \land \  a_{\text{\rm o}} < 0$.
\end{assumption}
\begin{assumption} \label{assm:3}
The other vehicle is located in front of the ego vehicle: $s_{0,\text{\rm e}} <
s_{0,\text{\rm o}}$.
\end{assumption}

\paragraph{Continuous dynamics.} As specified by the Vienna Convention on Road Traffic, 
the ego vehicle needs to avoid collision with the other vehicle when both 
vehicles are braking. To do so, the ego vehicle needs to \emph{predict} its own
braking movement and that of the other vehicle over time. We formalise the
prediction of this braking movement $p$ with a second-order ordinary
differential equation~(ODE)
\footnote{We use Lagrange's notation $f'$ and $f''$ to denote the first and the second derivative of $f$.} 
$p''(t) = a$ and initial
value conditions $p(0) = s_0$ and $p'(0) = v$.  The closed-form solution to this
ODE is as follows:
\begin{equation}
p(t)   \, := \,  s_0 + vt + \frac{1}{2} at^2 \label{eq:braking_movement}
\enspace .
%p'(t) & \, = \, & v + at\label{eq:velocity}
\end{equation}

\paragraph{Hybrid dynamics.} Since Eq.~\eqref{eq:braking_movement} is a
quadratic equation, it has the shape of a parabola when $a \neq 0$. This implies
that a vehicle would move backward after it stops.  Hence,
Eq.~\eqref{eq:braking_movement} is only valid for the interval $[0,
t_{\text{stop}}]$ where $t_{\text{stop}}$ is the stopping time.  The stopping time
$t_{\text{stop}}$ is the time when the first derivative of $p$ is zero, that is,
$p' (t_{\text{stop}})  \, = \, 0$.  Substituting $t$ with $t_{\text{stop}}$ in
the derivative of Eq.~\eqref{eq:braking_movement} results into the following
expression for $t_{\text{stop}}$:
\begin{equation}\label{eq:tstop}
    t_{\text{stop}} \, := \, - \frac{v}{a} \enspace .
\end{equation}
Thus, we can extend the movement $p$ of
Eq.~\eqref{eq:braking_movement} by introducing discrete jumps (the deceleration
makes a jump from $a < 0$ to $a = 0$) into the overall movement $s$
as follows.
\begin{equation}\label{eq:overall_movement}
s(t) \, := \,
   \begin{cases}
     s_0  & \text{if } \ t \, \le \, 0 \\
     p(t) & \text{if }\  0 \, \le \, t \, \le \, t_{\text{stop}} \\
     p(t_{\text{stop}}) & \text{if }\  t_{\text{stop}} \, \le \, t
   \end{cases}
\end{equation}
\paragraph{Two-vehicle scenario.} In Fig.~\ref{fig:scenario}, 
we assume that the other vehicle performs an emergency brake
with maximum deceleration $a_{\text{o}}$,
as specified in the Vienna Convention on Road Traffic.  As soon as the other vehicle brakes,
the ego vehicle reacts by performing an emergency brake too with maximum
deceleration~$a_{\text{e}}$.  Since an autonomous vehicle can react almost
instantly, we assume the reaction time to be zero.

In order to determine whether the distance $s_{\text{0,o}} - s_{\text{0,e}}$ is
safe or not, we first use Eq.~\eqref{eq:overall_movement} to predict the movement
of the ego vehicle $s_{\text{e}}(t)$ and the other vehicle $s_{\text{o}}(t)$ over time.
Then, a collision will occur if we can find future time $t$ such that $s_{\text{e}}(t) =
s_{\text{o}}(t)$. To generalise this predicate, we define $\Icollision$ over a
set of real numbers $T \, \subseteq \, \Real$ as follows:
\begin{equation}\label{eq:collision_def}
    \Icollision(T) \, :=  \, \left(\exists t \in T.~s_{\text{e}}(t) =
s_{\text{o}}(t)\right) \enspace .
\end{equation}
Equations~\eqref{eq:braking_movement} to \eqref{eq:overall_movement}, assumptions
\ref{assm:1} to \ref{assm:3}, and the definition in~\eqref{eq:collision_def} above
are our formalisation of the safe distance rule from the Vienna
Convention on Road Traffic. The remaining results presented in this paper
are deduced from there. The deductions are also formally checked by Isabelle
theorem prover.
