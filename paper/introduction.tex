\section{Introduction}
%Main motivation behind this paper
% law aspect --> to check compliance formally --> obscure / abstract predicate
% --> make it concrete
%(--> concrete notion can be used for verification (in the future!)
Liability is an important but rarely studied area in autonomous vehicle
technology. For example, who should be held liable when a collision involving an
autonomous vehicle occurs? In our previous paper~\cite{Rizaldi2015}, we proposed
to solve this issue by formalising vehicles' behaviours and traffic rules in
higher-order logic (HOL). This formalisation allows us to check formally whether
an autonomous vehicle complies with traffic rules. If autonomous
vehicles always comply with traffic rules, then they should not be held liable for
any accident.

One of the most important traffic rules is to maintain a safe distance between
a vehicle and the vehicle in front of it.  This notion of safe distance is
crucial for traffic simulation, 
automatic cruise controller (ACC), and safe intersections. 
Traffic simulation~\cite{Aghabayk2015} relies on this notion to
update the speed and acceleration of each vehicle such that a collision will
not occur in the simulation, even when the front vehicle brakes abruptly.
ACC~\cite{Xiao2010} and safe intersection systems~\cite{Loos2011,Kowshik2011}
rely on this notion to control the engine and brake module such that a rear-end
collision can be avoided.

The Vienna Convention on Road Traffic defines a `safe distance' as the distance
such that \emph{a collision between vehicles can be avoided if the vehicle in
front performs an emergency brake}~\cite{Vanholme2013}.  Note that this rule
states the requirement for safe distance descriptively; there is no
prescriptive expression against which a distance can be compared.  This makes
the process of formally checking the compliance of an autonomous vehicle's
behaviour with the safe distance rule problematic.

%General approach and introducing Isabelle
We follow our previous design decision~\cite{Rizaldi2015} to use
Isabelle/HOL~\cite{Paulson94} for three reasons.  Firstly, it has rich
libraries of formalised real analysis which is required to turn the descriptive
definition of safe distance into the prescriptive one. Secondly, it
allows us to generate code, which we use to evaluate a real data set.
Finally, as a theorem prover, Isabelle checks every reasoning step formally
and, hence, one only has to trust how we specify the notion of safe distance.
Our contributions are as follows:\footnote{Our formalisation is available
at \url{http://home.in.tum.de/~immler/safedistance/index.html}}
\begin{itemize}
\item We formalise a descriptive notion of safe distance from the Vienna
  Convention on Road Traffic (Sec.~\ref{sec:model}).
\item We turn this formalised descriptive definition of safe distance into
      a prescriptive one through logical analysis (Sec.~\ref{sec:case_analysis}).
\item We generate executable and formally verified checkers in SML for 
      validating the safe distance rule (Sec.~\ref{sec:checker}).
\item We evaluate the US Highway 101 data set from the Next Generation SIMulation
      (NGSIM) project as benchmark for our checkers (Sec.~\ref{sec:experiment}).
\item We argue that our prescriptive definition of \emph{safe distance} generalises
      all definitions of safe distance in the literature
      (Sec.~\ref{sec:related}).
\end{itemize}
We conclude and outline the possible extension of our work in
Sec.~\ref{sec:conclusion}.

